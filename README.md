# Build YoloV3 Locally Helper

1) Export Vott Project as CSV
2) Export Only tagged images
3) opener terminal and navigate to the parent folder of vott-csv-export

4) Run Command(WIndows may need tweak command by not using $PWD):

For GPU

docker run --runtime=nvidia -e MAX_BATCHES=100 -v $PWD/vott-csv-export:/training_data/vott-csv-export supremepokebotking/yolov3-local-helper-gpu

For CPU

docker run -e MAX_BATCHES=10 -v $PWD/vott-csv-export:/training_data/vott-csv-export supremepokebotking/yolov3-local-helper-cpu

-- Change Max batches to whatever you want. the default is 1000

5) If successful a zip called result.zip should be created in your export folder. 

Try the commands out in the attached vott export of cat photos.
